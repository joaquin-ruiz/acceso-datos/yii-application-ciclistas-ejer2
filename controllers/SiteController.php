<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\ContactForm;




class SiteController extends Controller
{
   
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionResult($idb, $method)
    {   
           $object = new \app\common\ExercisesStorage();
           $statements = $object->getEnunciation(); 
      
           $fields = $object->getFields();
           $sql = $object->getSQLString();               
    
        if ($method == 'ar') {
            $query = $object->getARQuery($idb);
             $provider = new ActiveDataProvider([
                'query' => $query,                   
                'pagination' => [
                    'pageSize' => 10,
                    ],
                ]);
             $method = 'Active Record';
        } else {        
            $query = $object->getDAOQuery($idb);  
            $provider = new SqlDataProvider([
                'sql' => $query,
                'totalCount' => Ciclista::$totalCiclistas,
                'pagination' => [
                    'pageSize' => 10,
                    ],
                ]);
            $method = 'Data Access Objects';
        }
        
        if ($idb == 4 ) {
             return $this->render("result",[
                "resultado" =>  $provider,
                "campos"    => ['edadMedia', 'nombresEquipo'],
                "title"     => "<h1 class='text-center'>Consulta Nº" . ($idb + 1) .
                               "</h1><h3 class='text-center'>" . $method . "</h3>",
                "enunciado" =>  $statements[$idb],
                "sql"       =>  $sql[$idb],
              ]);
            
        } elseif ($idb == 5){
            return $this->render("result",[
                "resultado" =>  $provider,
                "campos"    => ['totalCiclistas', 'nomequipo'],
                "title"     => "<h1 class='text-center'>Consulta Nº" . ($idb + 1) .
                               "</h1><h3 class='text-center'>" . $method . "</h3>",
                "enunciado" =>  $statements[$idb],
                "sql"       =>  $sql[$idb],
              ]);
            
            } elseif($idb == 10){ 
                return $this->render("result",[
                    "resultado" =>  $provider,
                    "campos"    => ['dorsal', 'numEtapas'],
                    "title"     => "<h1 class='text-center'>Consulta Nº" . ($idb + 1) .
                                   "</h1><h3 class='text-center'>" . $method . "</h3>",
                    "enunciado" =>  $statements[$idb],
                    "sql"       =>  $sql[$idb],
              ]);
              } else {
                    return $this->render("result",[
                        "resultado" =>  $provider,
                        "campos"    => [$fields[$idb]],
                        "title"     => "<h1 class='text-center'>Consulta Nº" . ($idb + 1) .
                                       "</h1><h3 class='text-center'>" . $method . "</h3>",
                        "enunciado" =>  $statements[$idb],
                        "sql"       =>  $sql[$idb],
              ]);
              }
        
         
    }
}
