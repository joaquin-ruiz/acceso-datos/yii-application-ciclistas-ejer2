<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\common;

use app\models\Ciclista;
use yii\base\BaseObject;
/**
 * Description of DataStorage
 *
 * @author Alienator
 */
class ExercisesStorage extends BaseObject {

    
  
    function __construct() {        
        parent::__construct();
    }

    public function init() {
        parent::init();
    }
    
    public function getEnunciation(){
        
       return array(
                    "-- (01) Número de ciclistas que hay",
                    "-- (02) Número de ciclistas que hay del equipo Banesto",
                    "-- (03) Edad media de los ciclistas",
                    "-- (04) La edad media de los del equipo Banesto",
                    "-- (05) La edad media de los ciclistas por cada equipo",
                    "-- (06) El número de ciclistas por equipo",
                    "-- (07) El número total de puertos",
                    "-- (08) El número total de puertos mayores de 1500",
                    "-- (09) Listar el nombre de los equipos que tengan más de 4"
                    . " ciclistas",
                    "-- (10) Listar el nombre de los equipos que tengan más de 4"
                    . " ciclistas cuya edad esté entre 28 y 32",
                    "-- (11) Indícame el número de etapas que ha ganado cada uno "
                    . "de los ciclistas",
                    "-- (12) Indícame el dorsal de los ciclistas que hayan ganado"
                    . " más de una etapa",       
        ); 
    }
    
    public function getARQuery($idb){
        switch ($idb) {
            case 0:
                $query = Ciclista::find()->select('COUNT(*) totalCiclistas')->distinct();
                break;
            case 1:
                $query = Ciclista::find()->select('COUNT(*) totalCiclistas')
                         ->where('nomequipo' == 'Banesto');
                break;
            case 2:
                // Error
                $query = Ciclista::find()->select('AVG(edad) edadMedia');
                break;
            case 3:
                $query = Ciclista::find()->select('AVG(edad) edadMedia')
                    ->where('nomequipo' == 'Banesto');  
                break;
            case 4:
               $query = Ciclista::find()->select('AVG(edad) edadMedia,'
                        . 'nomequipo nombresEquipo')
                        ->groupBy('nomequipo');                
                break;
            case 5:
                 $query = Ciclista::findBySql('COUNT(*) totalCiclistas, nomequipo');
                break;
            case 6:
                
                $query = Puerto::find() ->select("COUNT(*) totalPuertos");
                break;
            case 7:
               Puerto::find() ->select("COUNT(*) totalPuertos")->where("altura > 1500");
                break;
            case 8:
                Ciclista::find() ->select("nomequipo, COUNT(dorsal) totalCiclistas ")
                    ->groupBy("nomequipo")->having(" count(dorsal) > 4");
                break;
            case 9:
                Ciclista::find() ->select("nomequipo, COUNT(dorsal) totalCiclistas")
                    ->where("edad BETWEEN 28 and 32")->groupBy("nomequipo")
                    ->having(" count(dorsal) > 4");
                break;
            case 10:
                $query = Lleva::find()->select("dorsal, COUNT(numetapa) numEtapas")
                    ->groupBY(dorsal);                
                break; 
            case 11:
                $query = Lleva::find()->select("dorsal, COUNT(numetapa) numEtapas")
                    ->groupBY(dorsal)->having('numetapas > 1');               
                break;    
        }
    return $query;
    }
    
    public function getDAOQuery($idb){

        return $this->getSQLString()[$idb];
        //->queryScalar();  
    }
    
    public function getSQLString(){
        return array(
                "SELECT COUNT(*) totalCiclistas FROM `ciclista`",
                "SELECT COUNT(*) totalCiclistas FROM `ciclista` WHERE `ciclista`.`nomequipo` = 'Banesto'",
                "SELECT AVG(edad) edadMedia FROM `ciclista`",
                "SELECT AVG(edad) edadMedia FROM `ciclista` WHERE `ciclista`.`nomequipo` = 'Banesto'",
                "SELECT AVG(edad) edadMedia,`ciclista`.`nomequipo` nombresEquipo FROM `ciclista` "
                . "GROUP BY nombresEquipo",
                "SELECT COUNT(*) totalCiclistas, `ciclista`.`nomequipo` FROM `ciclista` "
                . "GROUP BY `nomequipo`",
                "SELECT COUNT(*) totalPuertos FROM `puerto`",
                "SELECT COUNT(*) totalPuertos FROM `puerto` WHERE `altura` > 1500",
                "SELECT `ciclista`.`nomequipo`, COUNT(nombre) totalCiclistas"
                . " FROM `ciclista` GROUP BY `ciclista`.`nomequipo`"
                . "HAVING totalciClistas > 4"
                . " ",            
                 "SELECT `ciclista`.`nomequipo`, COUNT(nombre) totalCiclistas"
                . " FROM `ciclista` WHERE `ciclista`.`edad` BETWEEN 28 AND 32 "
                . "GROUP BY `ciclista`.`nomequipo` HAVING totalCiclistas > 4",
                "SELECT `lleva`.`dorsal`, COUNT(`lleva`.`numetapa`) numEtapas "
                . "FROM `lleva` GROUP BY `lleva`.`dorsal`",
                "SELECT dorsal FROM etapa GROUP BY dorsal HAVING count(*) > 1"
        );
    }
    
    public function getFields() {        
       return array(
         'totalCiclistas',
                      'totalCiclistas',
                    'edadMedia',
                    'edadMedia',
                    null,
                       null,
                    'totalPuertos',
                        'totalPuertos',
                'nomequipo',
                'nomequipo',
                        null,
                    'dorsal',
        );
    }    
         
    
}
