<?php
use yii\helpers\Html;

$object = new app\common\ExercisesStorage();
$statements = $object->getEnunciation();

$ar_button = [    
    'class' => ['btn', 'btn-default'],
];
$dao_button = [
    'class' => ['btn', 'btn-primary']
];



/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">
        <div class="row">
            <?php               
                for ($i = 0; $i < sizeof($statements); $i++) {  
             ?>
            <div class="col-lg-4">
                <?= Html::tag('h2','Consulta ' . ($i + 1)) ?>
                <?= Html::tag('p', $statements[$i]); ?>               
                <?= Html::a('Boton AR', 'site/result?idb=' . $i . '&method=ar', $ar_button);?>
                <?= Html::a('Boton DAO', 'site/result?idb=' . $i . '&method=dao', $dao_button);?>                  
            </div>  
            <?php } ?>
        </div>
    </div>
</div>
